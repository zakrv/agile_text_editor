# Simple text editor
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Initial setup
Run `npm install` in order to setup application

## Text editor props
* `initialText`: string,
*  `buttonsConfig`: object,
*  `suggestions`: array,
*  `onTextSelection`: (string) => void 
*  `withResetFormat`: boolean

## Development and API server
Run `npm start` to concurrently starting CRA development and nodejs server for suggestions API call

## Notes
+ Text sample is given in `text.service.js`

