const express = require('express');
const cors = require('cors')
const fetch = require('node-fetch');

const PORT = process.env.PORT || 8080;

const server = express();

// const requestListener = async function (req, res) {
//     // const url_parts = url.parse(req.url, true);
//     // const query = url_parts.query;
//     // console.log(query);
//     switch(req.url){
//         case '/ml': {
//             const response = await fetch('https://api.datamuse.com/words?ml=designers');
//             const data = await response.json();
//             // // console.log(data);
//             // // res.end(data);
//             res.setHeader('Content-Type', 'application/json');
//             console.log(req.url);
//             res.end(JSON.stringify(data));
//         }
//         default: {
//             res.end('No endpoint found');
//         }
//     };
// };

// const server = http.createServer(requestListener);

server.get('/api/synonyms/:query', cors(), async (req, res) => {
    try {
        const query = req.params.query;
        const response = await fetch(`https://api.datamuse.com/words?ml=${query}`);
        const data = await response.json();
        res.status(200).json(data);
    } catch (e) {
        res.status(500).json({ message: 'Something went wrong, try again later' });
    }
});

server.listen(PORT, () => console.log(`Listening on ${PORT}`));
