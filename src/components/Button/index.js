import React from 'react';
import './styles.css';

const Button = ({ type = 'button', className='btn-primary', onClickHandler, children }) => {
    return (
        <button className={className} type={type} onClick={onClickHandler}>
            {children}
        </button>
    );
};

export default Button;
