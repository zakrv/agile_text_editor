import React, { useEffect, useCallback, useRef, useReducer, Fragment } from 'react';
import Popover from 'react-text-selection-popover';
import placeRightBelow from 'react-text-selection-popover/lib/placeRightBelow';
import Button from '../Button';
import {
    createIdGenerator,
    combineStyles,
    convertStyleArrToStr,
    getElementStringById,
    getSelectionProps,
    getLastSubstrIndex,
    isNumber,
} from './helpers';
import './styles.css';

const idGenerator = createIdGenerator();

const getId = () => idGenerator.next().value;

const getInitialState = (initialText) => ({
    currentStyles: {}, // object containing each span applied styles in format { id: [...stylesObjects]}
    editableValue: initialText || '', // value from typing event inside editable div
    editableHTML: initialText || '', // html which is dangerously inserted inside editable div
    selection: {
        prevSibling: null,
        selectedText: null,
        offset: null,
        selectedId: null,
    },
});

const reducer = (state, action) => {
    switch (action.type) {
        case 'UPDATE_EDITABLE': {
            const { type, editableHTML, styles = {}, selection = {} } = action;
            return {
                ...state,
                editableHTML,
                selection: {
                    ...state.selection,
                    ...selection,
                },
                currentStyles: {
                    ...state.currentStyles,
                    ...styles,
                },
            };
        }
        case 'UPDATE_EDITABLE_VALUE': {
            const { editableValue } = action;
            return {
                ...state,
                editableValue,
            };
        }
        case 'UPDATE_SELECTION': {
            const { selection } = action;
            return {
                ...state,
                selection: {
                    ...state.selection,
                    ...selection,
                },
            };
        }
        case 'RESET_SELECTION': {
            const { type, ...payload } = action;
            return {
                ...state,
                ...payload,
                selection: {},
            };
        }
        default: {
            return state;
        }
    }
};

const TextEditor = ({
    className = 'text-editor',
    buttonsConfig,
    initialText,
    withResetFormat = true,
    suggestions,
    onTextSelection,
}) => {
    const [
        {
            editableValue,
            editableHTML,
            currentStyles,
            selection: { prevSibling, selectedText, offset, selectedId },
        },
        dispatch,
    ] = useReducer(reducer, getInitialState(initialText));

    const divRef = useRef(null);

    useEffect(() => {
        if (editableHTML) {
            dispatch({
                type: 'UPDATE_EDITABLE_VALUE',
                editableValue: editableHTML,
            });
        }
    }, [editableHTML]);

    const handleInput = useCallback(
        (e) => {
            dispatch({
                type: 'UPDATE_EDITABLE_VALUE',
                editableValue: e.target.innerHTML,
            });
        },
        [dispatch]
    );

    const handleSelectionOffsets = useCallback(() => {
        const prevSpanOffset = prevSibling ? getLastSubstrIndex(editableValue, prevSibling.outerHTML) : 0;
        const editableBefore = editableValue.substr(0, offset + prevSpanOffset); // func ?
        const editableAfter = editableValue.substr(offset + prevSpanOffset, editableValue.length);
        return { editableBefore, editableAfter };
    }, [editableValue, offset, prevSibling]);

    const handleFormatting = useCallback(
        (style = {}, text) => {
            let updatedEditableHTML = '';
            let styles = {};
            let id = '';
            const currentElementString = getElementStringById(selectedId, editableValue);
            const textToReplace = text || selectedText;
            // handle already wrapped span
            if (currentElementString) {
                id = selectedId;
                // if reseting - just return empty array of styles
                styles = style === 'reset' ? [] : [...combineStyles(currentStyles[selectedId], style)];
                const combinedStylesString = convertStyleArrToStr(styles);
                const updatedElementString = `<span id="${selectedId}" class="selected" style="${combinedStylesString}">${textToReplace}</span>`;
                updatedEditableHTML = editableValue.replace(currentElementString, updatedElementString);
            } else {
                // handle first word wrappping with span
                id = getId();
                styles = style ? [style] : [];
                const styleString = style ? `${style.type}:${style.value}` : '';

                const { editableBefore, editableAfter } = handleSelectionOffsets();
                const replacedText = editableAfter.replace(
                    selectedText,
                    `<span id="${id}" class="selected" style="${styleString};">${textToReplace}</span>`
                );
                updatedEditableHTML = editableBefore.concat(replacedText);
            }
            dispatch({
                type: 'UPDATE_EDITABLE',
                editableHTML: updatedEditableHTML,
                styles: { [id]: styles },
                selection: { selectedId: id, selectedText: textToReplace },
            });
        },
        [currentStyles, editableValue, selectedId, selectedText, handleSelectionOffsets]
    );

    const handleResetFormat = useCallback(() => {
        if (isNumber(selectedId)) {
            handleFormatting('reset');
        }
    }, [selectedId, handleFormatting]);

    const handleMouseUp = useCallback(async () => {
        const selection = getSelectionProps(); // get selection from window.getSelection();
        if (selection.selectedText) {
            dispatch({
                type: 'UPDATE_SELECTION',
                selection,
            });
            onTextSelection(selection.selectedText);
        }
    }, [onTextSelection]);

    const handleMouseDown = useCallback(() => {
        if (selectedText) {
            dispatch({
                type: 'RESET_SELECTION',
                editableHTML: editableValue.replace('class="selected"', 'class=""'),
            });
        }
    }, [selectedText, editableValue]);

    const getActiveStatus = useCallback(
        ({ type, value }) => {
            // check if currentStyles by id exists
            if (currentStyles[selectedId]) {
                // check for both style type and value
                return currentStyles[selectedId].some((style) => style.type === type && style.value === value);
            }
            return false;
        },
        [currentStyles, selectedId]
    );

    const renderSuggestion = useCallback(
        (suggestion, i) => (
            <div className="suggestion" key={i} onClick={() => handleFormatting(null, suggestion)}>
                {suggestion}
            </div>
        ),
        [handleFormatting]
    );

    const renderButton = useCallback(
        ({ text, className = '', type, value }, index) => (
            <Button
                key={index}
                className={`${className} ${getActiveStatus({ type, value }) ? 'active' : ''} `}
                onClickHandler={() => handleFormatting({ type, value })}
            >
                {text}
            </Button>
        ),
        [getActiveStatus, handleFormatting]
    );

    return (
        <Fragment>
            <div className={className}>
                <Popover selectionRef={divRef} isOpen={!!selectedText} placementStrategy={placeRightBelow}>
                    <div className="text-editor-actions">
                        <div className="text-editor-styling">
                            {buttonsConfig.map(renderButton)}
                            {withResetFormat && (
                                <Button
                                    key="reset-format-button"
                                    className={`btn-secondary ${
                                        isNumber(selectedId) && currentStyles[selectedId].length > 0 ? 'active' : ''
                                    } `}
                                    onClickHandler={handleResetFormat}
                                >
                                    Reset Format
                                </Button>
                            )}
                        </div>
                        <div className="text-editor-suggestions">
                            {suggestions.isLoading ? (
                                <div className="suggestions-preloader">Loading suggestions ...</div>
                            ) : (
                                suggestions.data.length > 0 && (
                                    <div className="suggestions-scroll-container">
                                        {suggestions.data.map(renderSuggestion)}
                                    </div>
                                )
                            )}
                        </div>
                    </div>
                </Popover>
                <div
                    className="editable-div"
                    ref={divRef}
                    onInput={handleInput}
                    onMouseUp={handleMouseUp}
                    onMouseDown={handleMouseDown}
                    dangerouslySetInnerHTML={{ __html: editableHTML }}
                    contentEditable
                />
            </div>
        </Fragment>
    );
};

export default TextEditor;
