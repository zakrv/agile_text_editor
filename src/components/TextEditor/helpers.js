export const createIdGenerator = function* () {
    let id = 0;
    while (true) {
        yield id;
        id++;
    }
};

export const getLastSubstrIndex = (str, substr) => {
    const index = str.lastIndexOf(substr);
    if (index !== -1 && index > substr.length) {
        return index + substr.length;
    }
    return 0;
};

export const getSelectionProps = () => {
    const selectionObj = window.getSelection();
    return {
        selectedText: selectionObj.toString().trim(),
        selectedId:
            selectionObj.anchorNode &&
            selectionObj.anchorNode.parentElement &&
            selectionObj.anchorNode.parentElement.id,
        prevSibling: selectionObj.anchorNode && selectionObj.anchorNode.previousSibling,
        offset: window.getSelection().anchorOffset,
    };
};

export const getElementStringById = (id, string, tag = 'span') => {
    const startIndex = string.indexOf(`<${tag} id="${id}"`);
    if (startIndex !== -1) {
        const endIndex = string.substr(startIndex, string.length).indexOf(`</${tag}>`) + tag.length + 3;
        return string.substr(startIndex, endIndex);
    }
    return null;
};

export const convertStyleArrToStr = (styleArray) => {
    if (!styleArray) return null;
    let styleString = '';
    styleArray.forEach(({ type, value }) => (styleString = `${styleString}${type}:${value};`));
    return styleString;
};

export const combineStyles = (prev, next) => {
    if (!next) {
        // next style is not provided
        return prev;
    }
    // check if next style obj type exist inside prev ovj
    const isExist = prev.some((style) => style.type === next.type);
    if (isExist) {
        // if type exist, check if value is the same
        const isSameValue = prev.some((style) => style.value === next.value);
        if (isSameValue) {
            // if value is the same, then filter this style out
            return prev.filter((style) => style.type !== next.type);
        } else {
            // value is not the same, change old value on new one
            return prev.map((style) => (style.type === next.type ? next : style));
        }
    } else {
        // combine prev styles with next
        return [...prev, next];
    }
};

export const isNumber = (n) => !isNaN(parseFloat(n)) && !isNaN(n - 0);
