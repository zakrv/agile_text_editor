import React, { useEffect, useState, useReducer, useCallback } from 'react';
import { TextEditor } from './components';
import getMockText from './text.service';

const buttonsConfig = [
    {
        text: 'B',
        className: 'btn-primary bold',
        type: 'font-weight',
        value: 'bold',
    },
    {
        text: 'I',
        className: 'btn-primary italic',
        type: 'font-style',
        value: 'italic',
    },
    {
        text: 'U',
        className: 'btn-primary underline',
        type: 'text-decoration',
        value: 'underline',
    },
    {
        text: 'RED',
        className: 'btn-primary',
        type: 'color',
        value: 'red',
    },
    {
        text: 'BLUE',
        className: 'btn-primary',
        type: 'color',
        value: 'blue',
    },
    {
        text: 'GREEN',
        className: 'btn-primary',
        type: 'color',
        value: 'green',
    },
];

const reducer = (state, action) => {
    switch (action.type) {
        case 'START_SUGGESTIONS_FETCH': {
            return {
                ...state,
                isLoading: true,
                data: [],
            };
        }
        case 'FINISH_SUGGESTIONS_FETCH': {
            const { data } = action;
            return {
                ...state,
                isLoading: false,
                data,
            };
        }
        default: {
            return state;
        }
    }
};

const App = () => {
    const [initialText, setInitialText] = useState('');
    const [selection, setSelection] = useState('');
    const [suggestions, dispatch] = useReducer(reducer, { isLoading: false, data: [] });

    const handleSuggestion = useCallback(
        async (selection) => {
            dispatch({ type: 'START_SUGGESTIONS_FETCH' });
            try {
                const response = await fetch(`http://localhost:8080/api/synonyms/${selection}`);
                const data = await response.json();
                dispatch({ type: 'FINISH_SUGGESTIONS_FETCH', data: data.map((suggestion) => suggestion.word) });
            } catch (e) {
                dispatch({ type: 'FINISH_SUGGESTIONS_FETCH', data: [] });
            }
        },
        []
    );

    useEffect(() => {
        getMockText().then(setInitialText);
    }, []);

    useEffect(() => {
        if (selection) {
            handleSuggestion(selection);
        }
    }, [selection, handleSuggestion]);

    return (
        <div className="App">
            {initialText && (
                <TextEditor
                    initialText={initialText}
                    buttonsConfig={buttonsConfig}
                    suggestions={suggestions}
                    onTextSelection={setSelection}
                />
            )}
        </div>
    );
};

export default App;
